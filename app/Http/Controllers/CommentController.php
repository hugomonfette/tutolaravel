<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Resources\CommentResource;
use App\Http\Requests\CommentPostRequest;
use App\Http\Requests\CommentPutRequest;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Comment::paginate(5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CommentPostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentPostRequest $request)
    { 
        $comment = new Comment();
        $comment->text = $request->input('text');
        $comment->user_id = Auth::id();
        $comment->save();
        return $comment;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CommentPutRequest  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(CommentPutRequest $request, Comment $comment)
    {
        $comment = Comment::findOrFail($comment->id);
        $comment->text = $request->input('text');
        $comment->save();
        return $comment;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        if ($comment->delete()) {
            return new CommentResource($comment);
        }
    }
}
