<?php namespace App\Http\Controllers;

use App\Station;
use App\User;
use App\Http\Resources\StationResource;
use App\Http\Requests\StationPostRequest;
use App\Http\Requests\StationPutRequest;
use Illuminate\Support\Facades\Log;

class StationController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    *
    * @OA\Get(
    *       path="/api/stations",
    *       tags={"Station"},
    *       @OA\Parameter(
    *           name="page",
    *           description="Page number",
    *           required=false,
    *           in="path",
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
    *       @OA\Response(
    *           response="200",
    *           description="Returns the stations collection",
    *           @OA\JsonContent(
    *               type="array",
    *               @OA\Items(ref="#/components/schemas/Station")
    *           )
    *       )
    * )
    */
    public function index()
    {
        $stations = Station::paginate(5);
        return StationResource::collection($stations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *   tags={"Station"},
     *   path="/api/stations",
     *   security={ { "Oauth2Password": {} } },
     *   @OA\Response(
     *     response="201",
     *     description="Returns the created station",
     *     @OA\JsonContent(
     *       type="array",
     *       @OA\Items(ref="#/components/schemas/Station")
     *     )
     *   ),
     *   @OA\RequestBody(
     *     description="Station to create",
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
     *       @OA\Schema(ref="#/components/schemas/StationPostRequest")
     *     )
     *   )
     * )
     * 
     * @param  \App\Http\Requests\StationPostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StationPostRequest $request)
    {
        $station = new Station($request->all());
        if ($station->save()) {
            return new StationResource($station);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Station $station
     * @return \Illuminate\Http\Response
     */
    public function showStationById(Station $station)
    {
        return new StationResource($station);
    }

    public function showUsersStation(User $user)
    {
        $stations = Station::where('user_id', $user->id)->get();
        if ($stations->isEmpty()) {
            $json = ['message' => 'User does not have any stations.'];
                return response($json, 404);
        }
        for ($i = 0; $i < $stations->count(); $i++) {
            $response[$i] = new StationResource($stations[$i]);
        }

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Station $station
     * @return \Illuminate\Http\Response
     */
    public function update(StationPutRequest $request, Station $stations)
    {
        $station = Station::findOrFail($stations->id);

        $station->name = $request->input('name');
        $station->description = $request->input('description');
        $station->lat = $request->input('lat');
        $station->long = $request->input('long');
        
        $station->save();

        return new StationResource($station);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Station $station
     * @return \Illuminate\Http\Response
     */
    public function destroy(Station $stations)
    {
        if ($stations->delete()) {
            return new StationResource($stations);
        }
    }
}
