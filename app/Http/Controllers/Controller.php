<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @OA\Info(
 *      version="1.0.1",
 *      title="My First API",
 *      description="Tuto Laravel",
 *      @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
 * 
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Server"
 * )
 * 
 * @OA\SecurityScheme(
 *      type="oauth2",
 *      securityScheme="Oauth2Password",
 *      name="PasswordBased",
 *      scheme="bearer",
 *      description="Use client_id/client_secret and your username/password to get your token.",
 *      in="header",
 *      @OA\Flow(
 *          flow="password",
 *          authorizationUrl="/oauth/authorize",
 *          tokenUrl="/oauth/token",
 *          refreshUrl="/oauth/token/refresh",
 *          scopes={}
 *      )
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
