<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @OA\Schema(schema="Station")
 * {
 *   @OA\Property(
 *    property="name",
 *    type="string",
 *    description="The station name"
 *    ),
 *    @OA\Property(
 *       property="title",
 *       type="string",
 *       description="The station title"
 *    ),
 *    @OA\Property(
 *       property="lat",
 *       type="number",
 *       format="float",
 *       description="The station latitude"
 *    ),
 *    @OA\Property(
 *       property="long",
 *       type="number",
 *       format="float",
 *       description="The station longitude"
 *    )
 * }
 */

class StationResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'long' => $this->long,
            'lat' => $this->lat,
            'user_id' => $this->user_id,
        ];
    }
}
