<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
* @OA\Schema(schema="StationPostRequest")
* {
*   @OA\Property(
*     property="name",
*     type="string",
*     description="The station name"
*   ),
*   @OA\Property(
*     property="title",
*     type="string",
*     description="The station title"
*   )
* }
*/

class StationPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'filled|required|string',
            'description' => 'filled|string',
            'long' => 'filled|required|numeric',
            'lat' => 'filled|required|numeric',
            'user_id' => 'filled|required',
        ];
    }
}
