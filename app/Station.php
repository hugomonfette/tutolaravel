<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends \Eloquent
{
    protected $fillable = ['name', 'description', 'lat', 'long', 'user_id'];
    public $timstamps = true;
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function measure()
    {
        return $this->hasMany('App\Measure');
    }
}
