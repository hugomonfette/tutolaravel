<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measure extends Model
{
    protected $appends =['indice'];
    public $timstamps = true;
    
    // Access with : $model->indice;
    protected function getIndiceAttribute()
    {
        $color = "Brown";
        $label = "Dangerous";

        $value = $this->value;
        switch ($value)
        {
            case ($value <= 50) :
                $color = "Green";
                $label = "Good";
                break;
            case ($value <= 100) :
                $color = "Yellow";
                $label = "Average";
                break;
            case ($value <= 150) :
                $color = "Orange";
                $label = "Bad for sensible people";
                break;
            case ($value <= 200) :
                $color = "Red";
                $label = "Bad";
                break;
            case ($value <= 300) :
                $color = "Purple";
                $label = "Very bad";
                break;

        }
        return ['color'=>$color, 'label'=>$label];
    }

    public function station()
    {
        return $this->belongsTo('App\Station');
    }
}
