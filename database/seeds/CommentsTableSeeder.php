<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 100; $i++)
        {
            DB::table('comments')->insert([
                'user_id' => 1,
                'text' => 'Comment #' . $i
            ]);
        }
    }
}
