<?php

use Illuminate\Database\Seeder;

class MeasureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('measures')->insert([
            'station_id' => 1,
            'value' => 73,
            'description' => 'co2',
        ]);

        DB::table('measures')->insert([
            'station_id' => 1,
            'value' => 30,
            'description' => 'nh4',
        ]);

        DB::table('measures')->insert([
            'station_id' => 1,
            'value' => 8,
            'description' => 'nox',
        ]);

        DB::table('measures')->insert([
            'station_id' => 2,
            'value' => mt_rand(1, 100),
            'description' => 'co2',
        ]);

        DB::table('measures')->insert([
            'station_id' => 2,
            'value' => mt_rand(1, 100),
            'description' => 'h2o',
        ]);
    }
}
