<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use App\User;

class CommentTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetShouldReturn200()
    {
        $EXPECTED_STATUS_CODE = 200;

        $response = $this->JSON('GET', '/api/comments');

        $response->assertStatus($EXPECTED_STATUS_CODE);
    }

    public function testGetShouldReturnPaginatedList()
    {
        $EXPECTED_JSON_FRAGMENT = ['text' => 'Comment #1'];

        $response = $this->JSON('GET', '/api/comments');

        $response->assertJsonFragment($EXPECTED_JSON_FRAGMENT);
    }

    public function testGetShouldReturnPaginatedWithPageParameter()
    {
        $EXPECTED_JSON_FRAGMENT = ['text' => 'Comment #6'];

        $response = $this->JSON('GET', '/api/comments?page=2');

        $response->assertJsonFragment($EXPECTED_JSON_FRAGMENT);
    }

    public function testPostShouldReturnNewComment()
    {
        Passport::actingAs(
            User::find(1)
        );
        $EXPECTED_JSON_FRAGMENT = ['text' => 'newComment'];

        $response = $this->JSON('POST', '/api/comments', $EXPECTED_JSON_FRAGMENT);

        $response->assertJsonFragment($EXPECTED_JSON_FRAGMENT);
    }

    public function testPostShouldReturn201()
    {
        Passport::actingAs(
            User::find(1)
        );
        $JSON_FRAGMENT = ['text' => 'newComment'];
        $EXPECTED_STATUS_CODE = 201;

        $response = $this->JSON('POST', '/api/comments', $JSON_FRAGMENT);

        $response->assertStatus($EXPECTED_STATUS_CODE);
    }

    public function testPostShouldReturn422WhenTextFieldIsEmpty()
    {
        Passport::actingAs(
            User::find(1)
        );
        $JSON_FRAGMENT = ['text' => ''];
        $EXPECTED_STATUS_CODE = 422;

        $response = $this->JSON('POST', '/api/comments', $JSON_FRAGMENT);

        $response->assertStatus($EXPECTED_STATUS_CODE);
    }

    public function testPostShouldReturnErrorMessageWhenTextFieldIsEmpty()
    {
        Passport::actingAs(
            User::find(1)
        );
        $JSON_FRAGMENT = ['text' => ''];
        $EXPECTED_JSON_FRAGMENT = ['The text field is required.'];

        $response = $this->JSON('POST', '/api/comments', $JSON_FRAGMENT);

        $response->assertJsonFragment($EXPECTED_JSON_FRAGMENT);
    }

    public function testPutShouldUpdateComment()
    {
        Passport::actingAs(
            User::find(1)
        );
        $JSON_FRAGMENT = ['text' => 'newCommentText'];

        $response = $this->JSON('PUT', '/api/comments/1', $JSON_FRAGMENT);

        $response->assertJsonFragment($JSON_FRAGMENT);
    }

    public function testPutShouldReturn200()
    {
        Passport::actingAs(
            User::find(1)
        );
        $JSON_FRAGMENT = ['text' => 'newCommentText'];

        $response = $this->JSON('PUT', '/api/comments/1', $JSON_FRAGMENT);

        $response->assertStatus(200);
    }

    public function testPutShouldReturnErrorMessageWhenUserIsUnauthenticated()
    {
        $JSON_FRAGMENT = ['text' => 'newCommentText'];

        $response = $this->JSON('PUT', '/api/comments/1', $JSON_FRAGMENT);

        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }

    public function testPutShouldReturn401WhenUserIsUnauthenticated()
    {
        $JSON_FRAGMENT = ['text' => 'newCommentText'];

        $response = $this->JSON('PUT', '/api/comments/1', $JSON_FRAGMENT);

        $response->assertStatus(401);
    }

    public function testPutShouldReturnErrorMessageWhenUserIsNotOwner()
    {
        Passport::actingAs(
            User::find(2)
        );
        $JSON_FRAGMENT = ['text' => 'newCommentText'];

        $response = $this->JSON('PUT', '/api/comments/1', $JSON_FRAGMENT);

        $response->assertJsonFragment(['message' => 'Unauthorized action.']);
    }

    public function testPutShouldReturn403WhenUserIsNotOwner()
    {
        Passport::actingAs(
            User::find(2)
        );
        $JSON_FRAGMENT = ['text' => 'newCommentText'];

        $response = $this->JSON('PUT', '/api/comments/1', $JSON_FRAGMENT);

        $response->assertStatus(403);
    }

    public function testPutShouldReturnErrorMessageWhenTextFieldIsEmpty()
    {
        Passport::actingAs(
            User::find(1)
        );
        $JSON_FRAGMENT = ['text' => ''];

        $response = $this->JSON('PUT', '/api/comments/1', $JSON_FRAGMENT);

        $response->assertJsonFragment(['The text field is required.']);
    }

    public function testPutShouldReturn422WhenTextFieldIsEmpty()
    {
        Passport::actingAs(
            User::find(1)
        );
        $JSON_FRAGMENT = ['text' => ''];

        $response = $this->JSON('PUT', '/api/comments/1', $JSON_FRAGMENT);

        $response->assertStatus(422);
    }

    public function testDeleteShouldReturn404WhenIdIsInvalid()
    {
        Passport::actingAs(
            User::find(1)
        );

        $response = $this->JSON('DELETE','/api/comments/-1');

        $response->assertStatus(404);
    }

    public function testDeleteShouldDeleteCommentWhenIdIsValid()
    {
        Passport::actingAs(
            User::find(1)
        );
        $JSON_FRAGMENT = ['text' => 'newComment'];
        $newComment = $this->JSON('POST', '/api/comments', $JSON_FRAGMENT);
        $newId = json_decode($newComment->getContent())->id;

        $this->JSON('DELETE', '/api/comments/' . $newId);

        $response = $this->JSON('GET','/api/stations/' . $newId);
        $response->assertStatus(404);
    }

    public function testDeleteShouldReturn200WhenIdIsValid()
    {
        Passport::actingAs(
            User::find(1)
        );
        $JSON_FRAGMENT = ['text' => 'newComment'];
        $newComment = $this->JSON('POST', '/api/comments', $JSON_FRAGMENT);
        $newId = json_decode($newComment->getContent())->id;

        $response = $this->JSON('DELETE', '/api/comments/' . $newId);

        $response->assertStatus(200);
    }

    public function testDeleteShouldReturnErrorMessageWhenUserIsUnauthenticated()
    {
        $response = $this->JSON('DELETE', '/api/comments/1');

        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }

    public function testDeleteShouldReturn401WhenUserIsUnauthenticated()
    {
        $response = $this->JSON('DELETE', '/api/comments/1');

        $response->assertStatus(401);
    }

    public function testDeleteShouldReturnErrorMessageWhenUserIsNotOwner()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        $response = $this->JSON('DELETE', '/api/comments/1');

        $response->assertJsonFragment(['message' => 'Unauthorized action.']);
    }

    public function testDeleteShouldReturn403WhenUserIsNotOwner()
    {
        Passport::actingAs(
            \App\User::find(2)
        );

        $response = $this->JSON('DELETE', '/api/comments/1');

        $response->assertStatus(403);
    }
}
