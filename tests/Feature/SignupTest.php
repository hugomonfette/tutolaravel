<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SignupTest extends TestCase
{
    use DatabaseTransactions;

    const DEFAULT_NAME = 'default';
    const DEFAULT_EMAIL = 'default@email.com';
    const DEFAULT_PASSWORD = 'default';
    const DEFAULT_TOKEN_HEADER = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6I';

    const DEFAULT_UNAUTH_EMAIL = 'unauth@email.com';
    const DEFAULT_UNAUTH_PASSWORD = 'unauth';

    public function testPostSignup()
    {
        $response = $this->JSON('POST', 'api/register', [
                                            'name' => self::DEFAULT_NAME,
                                            'email' => self::DEFAULT_EMAIL,
                                            'password' => self::DEFAULT_PASSWORD
        ]);

        $response->assertSeeText("token");
        $response->assertSeeText(self::DEFAULT_TOKEN_HEADER);
        $response->assertStatus(200);
    }

    public function testPostSigninShouldReturn200()
    {
        $response = $this->JSON('POST', 'api/login', [
                                            'email' => 'admin@api.com',
                                            'password' => 'admin'
        ]);

        $response->assertStatus(200);
    }

    public function testPostSigninInvalidUserShouldReturn401()
    {
        $response = $this->JSON('POST', 'api/login', [
                                            'username' => self::DEFAULT_EMAIL,
                                            'password' => self::DEFAULT_PASSWORD
        ]);

        $response->assertStatus(401);
    }

    public function testPostSigninShouldReturn429WhenTooManyAttempts()
    {
        $INVALID_EMAIL = 'invalid@email.com';
        $INVALID_PASSWORD = 'invalidPassword';
        $THROTTLING_LIMIT = 5;

        for ($i = 0; $i <= $THROTTLING_LIMIT; $i++)
        {
            $response = $this->JSON('POST', 'api/login', [
                'email' => $INVALID_EMAIL,
                'password' => $INVALID_PASSWORD]);
        }

        $response->assertStatus(429);
        $response->assertHeader("X-RateLimit-Limit");
        $response->assertHeader("X-RateLimit-Remaining");
    }
}
